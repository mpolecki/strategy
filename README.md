# Wzorzec Adapter

## 1. Zastosowanie
Wzorzec używa się gdy chcemy wykonać pewien algorytm w różny sposób. Najprościej użyć wówczas wskaźnika na jeden rodzaj funkcji, który możemy sobie dowolnie modyfikować.

## 2. Przykład użycia
Posiadamy różne media komunikacyjne i chcemy wysyłać na nie dane z czujników. Jedyne co nas interesuje to tylko podanie z jakie medium chcemy skorzystać za pomocą wskaźnika na funkcję.

- Definiujemy wskaźnik na funkcję, która będzie przyjmowała jako argument wskaźnik na dane oraz ich ilość:
	```c
	typedef void (*send_data_t)(uint8_t *, uint8_t);

	send_data_t send_data_callback = uart_send_data;
	```

- W zależności od wybranego medium zmieniamy wskaźnik na funkcję na wartość odpowiedniej funkcji

	```c
	  switch(medium)
	  {
	  case UART_MEDIUM:
		  send_data_callback = uart_send_data;
		  break;

	  case CAN_MIEDUM:
		  send_data_callback = can_send_data;
		  break;

	  case USB_MEDIUM:
		  send_data_callback = usb_send_data;
		  break;

	  default:
		  break;
	  }

	  send_data_callback(last_data.data_pnt, last_data.data_size);
	```

## 3. Przebieg programu:
ODczytujemy dane z czujnika, wybieramy medium, a następnie wywołujemy tę funkcję za pomocą wskaźnika na nią, a także zmieniamy medium dla kolejnej iteracji.
```c
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  last_data = get_data_from_sensor();

	  switch(medium)
	  {
	  case UART_MEDIUM:
		  send_data_callback = uart_send_data;
		  break;

	  case CAN_MIEDUM:
		  send_data_callback = can_send_data;
		  break;

	  case USB_MEDIUM:
		  send_data_callback = usb_send_data;
		  break;

	  default:
		  break;
	  }

	  send_data_callback(last_data.data_pnt, last_data.data_size);

	  change_medium();

	  HAL_Delay(1000);

  }
```

